STO\_main.tests package
=======================

Submodules
----------

STO\_main.tests.test\_forms module
----------------------------------

.. automodule:: STO_main.tests.test_forms
   :members:
   :undoc-members:
   :show-inheritance:

STO\_main.tests.test\_models module
-----------------------------------

.. automodule:: STO_main.tests.test_models
   :members:
   :undoc-members:
   :show-inheritance:

STO\_main.tests.test\_views module
----------------------------------

.. automodule:: STO_main.tests.test_views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: STO_main.tests
   :members:
   :undoc-members:
   :show-inheritance:
