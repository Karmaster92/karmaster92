STO\_main.templatetags package
==============================

Submodules
----------

STO\_main.templatetags.custom\_tags module
------------------------------------------

.. automodule:: STO_main.templatetags.custom_tags
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: STO_main.templatetags
   :members:
   :undoc-members:
   :show-inheritance:
