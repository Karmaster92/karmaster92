# Generated by Django 4.1.1 on 2022-10-10 15:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('STO_main', '0030_alter_service_max_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='body',
            field=models.TextField(verbose_name='описание услуги'),
        ),
    ]
