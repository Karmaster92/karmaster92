# Generated by Django 4.1.1 on 2022-09-28 09:41

import STO_main.models
import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('STO_main', '0004_case_end_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='STOStaff',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150, unique=True, verbose_name='Название')),
                ('slug', models.SlugField(max_length=255, unique=True, verbose_name='URL')),
                ('create_time', models.DateTimeField(default=datetime.datetime.now, verbose_name='время создания')),
                ('update_time', models.DateTimeField(default=datetime.datetime.now, verbose_name='время изменения')),
                ('is_active', models.BooleanField(db_index=True, default=True, verbose_name='активен')),
                ('first_name', models.CharField(max_length=50, verbose_name='имя')),
                ('last_name', models.CharField(max_length=50, verbose_name='фамилия')),
                ('patronymic', models.CharField(max_length=50, verbose_name='отчество')),
                ('position', models.CharField(max_length=100, verbose_name='должность')),
                ('range', models.PositiveIntegerField(default=1, verbose_name='определение порядка вывода')),
                ('about', models.CharField(max_length=150, verbose_name='о сотруднике')),
                ('image', models.ImageField(blank=True, upload_to=STO_main.models.form_image_path, verbose_name='фото сотрудника')),
            ],
            options={
                'verbose_name': 'Сотрудник',
                'verbose_name_plural': 'Сотрудники',
                'ordering': ('range',),
            },
        ),
    ]
