# Generated by Django 4.1.1 on 2022-10-02 17:46

import STO_main.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('STO_main', '0014_stock_is_indefinite_alter_stock_end_date_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stock',
            name='image',
            field=models.ImageField(blank=True, upload_to=STO_main.models.form_image_path, verbose_name='баннер'),
        ),
    ]
