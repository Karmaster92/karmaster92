"""
The submodule contains representations for implementing the target logic of the project.

Here are the views:

    * to handle server 404 and 500 errors;
    * for the main page;
    * to form pages of categories, subcategories with the output of services belonging to them;
    * for the specific service page;
    * for the page of examples of completed orders:
    * for the specific completed orders page:
    * for the About Us page with a list of current managers and employees;
    * for the page of video reviews from customers:
    * for the page with a list of guarantees;
    * for the service search page with the output of services filtered by the search mask;
    * to add and remove services to the list of services of interest to the site visitor (with the storage of this list in the session);
    * for the contacts page with the feedback form;
    * for a pre-registration page with an appropriate form and the ability to work with a list of services of interest to the user;
"""
from functools import reduce
import logging
from logging import Logger

from django.contrib import messages
from django.core.mail import send_mail
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils import timezone
from django.views import View
from django.views.decorators.http import require_GET
from django.views.generic import TemplateView, DetailView, ListView
from django.views.generic.edit import FormMixin

from STO_main.forms import ContactUsForm
from STO_main.mixins import TitleMixin
from STO_main.models import Category, Service, Case, STOStaff, VideoFile, Stock, ClientMessages
from STO_server.settings import DOMAIN_NAME, EMAIL_HOST_USER

logger: Logger = logging.getLogger(__name__)


@require_GET
def robots_txt(request):
    """Formation of robots.txt."""
    lines = [
        "User-Agent: *",
        "Disallow: /private/",
        "Disallow: /junk/",
    ]
    return HttpResponse("\n".join(lines), content_type="text/plain")


def my_handler404(request, exception):
    """View for the 404 page."""
    context = {'seo_title': '404: Страница не существует',
               'problem': 'К сожалению, такой страницы не существует.'}
    response = render(request, '404.html', context=context)
    response.status_code = 404
    return response


def my_handler500(request, exception):
    """View for the 500 page."""
    context = {'seo_title': '500: Внутренняя ошибка сервера',
               'problem': 'Извините, произошла внутренняя ошибка сервера. Скоро мы все исправим.'}
    response = render(request, '404.html', context=context)
    response.status_code = 500
    return response


class MainView(TitleMixin, TemplateView):
    """View for the main page."""
    template_name = 'index.html'
    seo_title = 'Ремонт автомобилей в Севастополе, бесплатная диагностика на СТО «Кармастер».'
    seo_description = 'СТО «Кармастер» - профессиональный ремонт и ' \
                      'диагностика автомобилей в Севастополе по доступным ценам.'

    def get_context_data(self, **kwargs):
        """Adds a queryset of the last 6 completed works to the context"""
        context = super().get_context_data()
        context['last_cases'] = Case.objects.filter(is_active=True)[:6]
        return context


class CategoryView(TitleMixin, ListView):
    """View for subcategories or services of a separate category (section)."""
    model = Service
    template_name = 'services/categories-list.html'
    paginate_by = 6
    slug_url_kwarg = 'category_slug'
    category = 'all'

    def get_queryset(self):
        """Returns a queryset of active services for the selected category.
        If root category section chosen, an empty queryset will be returned
        (and the main categories will be shown)."""
        current_slug = self.kwargs.get('category_slug')
        if current_slug == 'all':
            return Service.objects.none()
        self.category = Category.objects.get(slug=current_slug)
        return Service.objects.filter(Q(category=self.category), Q(is_active=True))


    def get_context_data(self, *args, **kwargs):
        """Puts the title in the context according to the selected category."""
        context = super().get_context_data(**kwargs)
        context['title'] = f'Услуги категории {self.category}'\
            if not self.category == 'all' else 'Все категории услуг'
        if self.category == 'all':
            context['all'] = Category.objects.filter(is_active=True)
        else:
            context['category'] = self.category
        return context

    def get_page_title(self, context):
        """Returns page title value."""
        if self.category != 'all':
            return self.category.seo_title if self.category.seo_title else self.category.title
        return 'Услуги СТО "Кармастер" в Севастополе'

    def get_page_description(self, context):
        """Returns page description value."""
        if self.category != 'all':
            return self.category.seo_description if self.category.seo_description else ''
        return 'Ремонт автомобилей недорого в Севастополе'


class ServiceView(TitleMixin, DetailView):
    """View for the specific service."""
    model = Service
    template_name = 'services/service-details.html'
    slug_url_kwarg = 'service_slug'

    def get_page_title(self, context):
        """Returns page title value."""
        return context['service'].seo_title if context['service'].seo_title else \
            context['service'].title

    def get_page_description(self, context):
        """Returns page description value."""
        return context['service'].seo_description if context['service'].seo_description else ''

    def get_object_title(self, context):
        """Returns object title value."""
        return context['service'].title


class CaseListView(TitleMixin, ListView):
    """View for all completed orders (cases)."""
    model = Case
    template_name = 'cases/cases-list.html'
    paginate_by = 2
    title = 'Выполненные работы'
    seo_title = 'Наши работы - примеры отремонтированных машин на СТО «Кармастер» в Севастополе'
    seo_description = 'Примеры работ профессионального автосервиса «Кармастер» в Севастополе.'

    def get_queryset(self):
        """Returns a queryset of all active cases."""
        return Case.objects.filter(is_active=True).defer('services', 'price',
                                                         'car', 'create_time',
                                                         'update_time')


class CaseView(TitleMixin, DetailView):
    """View for the specific case."""
    model = Case
    template_name = 'cases/case-details.html'
    slug_url_kwarg = 'case_slug'

    def get_page_title(self, context):
        """Returns page title value."""
        return context['case'].seo_title if context['case'].seo_title else context['case'].title

    def get_page_description(self, context):
        """Returns page description value."""
        return context['case'].seo_description if context['case'].seo_description else ''

    def get_object_title(self, context):
        """Returns object title value."""
        return f'Наша работа: {context["case"].title}'

    def get_context_data(self, *args, **kwargs):
        """Adds the last 3 completed cases to the context, excluding the current one."""
        context = super().get_context_data(**kwargs)
        context['other_cases'] = Case.objects.filter(is_active=True).exclude(
            slug=kwargs.get('object').slug)[:3]
        return context


class StockListView(TitleMixin, ListView):
    """View for all stocks."""
    model = Stock
    template_name = 'stocks/stocks-list.html'
    paginate_by = 3
    title = 'Акции и бонусы'
    seo_title = 'Акции и скидки на недорогой ремонт автомобилей в Севастополе на СТО «Кармастер».'
    seo_description = 'СТО «Кармастер» регулярно проводит акции и предоставляет скидки на ремонт авто своим клиентам.'

    def get_queryset(self):
        """Returns a queryset of all active stocks."""
        return Stock.objects.filter(is_active=True)


class StockView(TitleMixin, DetailView):
    """View for the specific stock."""
    model = Stock
    template_name = 'stocks/stock-details.html'
    slug_url_kwarg = 'stock_slug'

    def get_page_title(self, context):
        """Returns page title value."""
        return context['stock'].seo_title if context['stock'].seo_title else context['stock'].title

    def get_page_description(self, context):
        """Returns page description value."""
        return context['stock'].seo_description if context['stock'].seo_description else ''

    def get_object_title(self, context):
        """Returns object title value."""
        return context['stock'].title

    def get_context_data(self, *args, **kwargs):
        """Adds the last 3 active stocks to the context, excluding the current one."""
        context = super().get_context_data(**kwargs)
        stock = kwargs.get('object')
        context['other_stocks'] = Stock.objects.filter(is_active=True).exclude(slug=stock.slug)[:3]
        return context


class BonusProgramView(TitleMixin, TemplateView):
    """View for Bonus program page."""
    template_name = 'stocks/bonus.html'
    title = 'Бонусная программа от Кармастер'
    seo_title = 'Бонусная программа СТО «Кармастер» в Севастополе'
    seo_description = 'Условия бонусной программы СТО «Кармастер» ва Севастополе.'


class ReferralProgramView(TitleMixin, TemplateView):
    """View for the Referral program page."""
    template_name = 'stocks/referral.html'
    title = 'Реферальная программа от Кармастер'
    seo_title = 'Получите бонусы за привлеченных на СТО клиентов'
    seo_description = 'Реферальная программа для клиентов нашего СТО. Рекомендуйте нас и получайте бонусы.'


class StaffListView(TitleMixin, ListView):
    """View for About page."""
    model = STOStaff
    template_name = 'about/about.html'
    title = 'О нас'
    seo_title = 'Официальный сайт СТО «Кармастер» в Севастополе. ' \
                'Профессиональный автосервис по доступным ценам.'
    seo_description = 'Подробная информация об СТО «Кармастер» в Севастополе. ' \
                      'Профессиональный автосервис по доступным ценам.'

    def get_queryset(self):
        """Returns a queryset of active staff."""
        return STOStaff.objects.filter(is_active=True)


class VideoReviewsListView(TitleMixin, ListView):
    """View for video-reviews page."""
    model = VideoFile
    template_name = 'about/reviews-list.html'
    paginate_by = 2
    title = 'Отзывы о Кармастер'
    seo_title = 'Отзывы о СТО «Кармастер» в Севастополе - мнения клиентов об автосервисе'
    seo_description = 'Отзывы клиентов о СТО «Кармастер» в Севастополе. ' \
                      'Мнения и впечатления об автосервисе.'

    def get_queryset(self):
        """Returns a queryset of active video-reviews."""
        return VideoFile.objects.filter(is_active=True).defer('create_time',
                                                              'update_time', 'person')


class GuaranteeView(TitleMixin, TemplateView):
    """View for the Guarantees page."""
    template_name = 'about/guarantees.html'
    title = 'Гарантии Кармастер'
    seo_title = 'Ремонт автомобилей в Севастополе с гарантией -  СТО «Кармастер» в Севастополе'
    seo_description = 'Условия гарантии на ремонт автомобилей на  СТО «Кармастер» в Севастополе.'


class ServicesFilteredView(ListView):
    """View for a page of services filtered by the user's request."""
    model = Service
    template_name = 'services/service-filtered.html'
    paginate_by = 6

    def get_queryset(self):
        """Returns a queryset of services according to the user's request (filter).
        If the request was not passed, the last filter stored in the session will be used."""
        query = self.request.GET.get('search_panel')
        if query:
            self.request.session['filter'] = query
        if 'filter' in self.request.session:
            query = self.request.session['filter']
        context_object_name = Service.objects.filter(title__icontains=query,
                                                     is_active=True)
        return context_object_name

    def get_context_data(self, **kwargs):
        """Adds the page title to the context.
        Saves the user's last request (filter) in the session."""
        context = super().get_context_data(**kwargs)
        try:
            user_filter = self.request.session['filter']
            if user_filter == '':
                raise KeyError
            context['title'] = f'Поиск услуг по фразе "{user_filter}"'
        except KeyError:
            context['title'] = 'Чтобы выполнить поиск, задайте поисковую фразу'
            self.request.session['filter'] = ''
        return context


class AddServiceToMyListView(View):
    """Saves the service selected by the user to the list of his interests,
    which is stored in the session."""

    def post(self, request, *args, **kwargs):
        """Adds the selected service to the list of the user's interests.
        Returns a redirect to the current page. If the user has added
        a service on a page with a list of services,
        redirect will be performed directly to this service using the html anchor mechanism."""
        if 'user_services' not in self.request.session:
            self.request.session['user_services'] = []
        chosen_service = Service.objects.get(slug=kwargs['slug'])
        if chosen_service.slug not in self.request.session['user_services']:
            self.request.session['user_services'].append(chosen_service.slug)
            request.session.modified = True
        try:
            if 'service_page' in request.POST:
                return redirect(reverse_lazy('service_read',
                                             kwargs={'service_slug': chosen_service.slug}))
            page = request.POST['path'].split('?')[1]
            return redirect(reverse_lazy('category_read',
                                         kwargs={'category_slug': chosen_service.category.slug})
                            + f'?{page}#to_{chosen_service.slug}')
        except IndexError:
            return redirect(reverse_lazy('category_read',
                                         kwargs={'category_slug': chosen_service.category.slug})
                            + f'#to_{chosen_service.slug}')


class RemoveChosenService(View):
    """Removes the service selected by the user from the list of his interests,
    which is stored in the session."""

    def post(self, request, *args, **kwargs):
        """Removes the selected service from the user's list of interests.
        Returns a redirect to the current page."""
        self.request.session['user_services'].remove(kwargs['slug'])
        request.session.modified = True
        return redirect(reverse_lazy('services_registration'))


class ContactUsView(TitleMixin, FormMixin, TemplateView):
    """View for the Contact us page."""
    template_name = 'about/contact.html'
    form_class = ContactUsForm
    success_url = reverse_lazy('index')
    title = 'Связаться с Кармастер'
    seo_title = 'Контактная информация СТО «Кармастер» в Севастополе'
    seo_description = 'Контактная информация СТО «Кармастер» в Севастополе. ' \
                      'Телефоны, адрес, расположение на карте.'

    def post(self, request, *args, **kwargs):
        """Receives the form data entered by the user, puts them in the context
        for the formation of an email and sends it to the mail linked to this site."""
        form = self.form_class(data=request.POST)
        context = self.get_context_data()
        user_services = context['services'] if 'services' in context else None
        if form.is_valid():
            try:
                subject = 'Новое сообщение от клиента для Кармастер92'
                context = {'name': request.POST['name'], 'my_site_name': DOMAIN_NAME,
                           'content': request.POST['content'], 'phone': request.POST['phone'],
                           'form_date': timezone.now(), 'form_type': 'обратной связи',
                           'user_services': user_services,
                           }
                message = render_to_string('new-appointment-email.html', context)

                self.message_instance_create(request)

                send_mail(subject, message, EMAIL_HOST_USER, [EMAIL_HOST_USER],
                          html_message=message, fail_silently=False)
                messages.success(request, 'Ваше сообщение успешно отправлено.'
                                          'Мы свяжемся с вами в ближайшее время.')
                return render(request, self.template_name, context)
            except Exception as error:
                logger.error('Ошибка отправки сообщения - %s', error)
                form.add_error(None, 'Ошибка отправки сообщения')
        context['form'] = form
        return render(request, self.template_name, context)

    def message_instance_create(self, request):
        """Saves in the database an instance of the ClientMessages model
        with the data of the completed form."""
        new_msg = ClientMessages.objects.create(body=request.POST['content'],
                                                user=request.POST['name'],
                                                create_time=timezone.now(),
                                                phone=request.POST['phone'], )
        return new_msg


class AppointmentView(ContactUsView):
    """View for the Appointment page."""
    template_name = 'about/appointment.html'
    title = 'Записаться на СТО'
    seo_title = 'Записаться на ремонт автомобиля в Севастополе на СТО «Кармастер».'
    seo_description = 'Страница для предварительной записи на ремонт или диагностику автомобиля ' \
                      'на СТО «Кармастер» в Севастополе.'

    def get_context_data(self, **kwargs):
        """Adds to the context the services selected by the user as interesting to him
        and the amount of their minimum cost."""
        context = super().get_context_data()
        if 'user_services' in self.request.session:
            context['services'] = Service.objects.filter\
                (slug__in=self.request.session['user_services'])
            context['services_sum'] = \
                sum(list(map(lambda x: x.min_price, context['services'])))
                # reduce(lambda x, y: x + y, [x.min_price for x in context['services']])
        return context

    def message_instance_create(self, request):
        """Saves in the database an instance of the ClientMessages model with
        the data of the completed form.
        Adds the selected services to the instance (Many-to-Many field).
        Clears the list of services that the user is interested in.
        """
        context = self.get_context_data()
        new_msg = super().message_instance_create(request)
        if 'services' in context:
            new_msg.chosen_services.add(*[i.id for i in context['services']])
            del self.request.session['user_services']
