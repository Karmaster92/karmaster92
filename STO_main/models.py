"""
Contains the models required to ensure the operation of the main logic of the project.
"""

import logging
from datetime import datetime
from logging import Logger
from django.db import IntegrityError

from PIL import Image
from django.core.validators import RegexValidator
from django.db import models, transaction
from django.db.models import F
from django.urls import reverse
from django.utils import timezone
from django.utils.timezone import localdate
from embed_video.fields import EmbedVideoField
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from phonenumber_field.modelfields import PhoneNumberField
from pytils.translit import slugify

logger: Logger = logging.getLogger(__name__)

# validator for vehicle VIN and license plates
alphanumeric = RegexValidator(r'^[0-9a-zA-ZА-Яа-я]*$',
                              'Допустимы только буквенно-цифровые значения.')


def form_image_path(instance, filename: str, folder: str = 'all') -> str:
    """Generates and returns the path for the saved images.
    It is necessary for the orderly storage of images.

    Args:

        * instance: an instance of an object being created or modified.
        * filename (`str`): the name under which the image will be stored.
        * folder (`str`): name of the folder where the image will be placed.

    Returns:

        * str: the path to the saved file.
    """
    folders = {Case: 'case_images', Category: 'cat_images',
               Stock: 'stock_images', STOStaff: 'staff_images'}
    try:
        title_str = instance.title.replace(' ', '_')
        if type(instance) == Service:
            return f'service_images/{instance.category}/{title_str}_{filename}'
        folder = folders[type(instance)]
        return f'{folder}/{title_str}_{filename}'
    except AttributeError:
        try:
            folder = folders[type(instance)]
            return f'{folder}/{instance.first_name}_{instance.last_name}_{filename}'
        except Exception as error:
            logger.error('Error when forming the path to save the image %s'
                         'when %s creating.\n%s', filename, instance, error)
    except Exception as error:
        logger.error('Error when forming the path to save the image %s'
                     'when %s creating.\n%s', filename, instance, error)
    return f'{folder}/error_path/{filename}'


def image_processing(image, max_width: int, max_height: int):
    """Reduces images to a required size, if they exceed it.

    Args:

        * image: contents of the image field of instance.
        * max_width (`int`): set maximum width.
        * max_height (`int`): set maximum height.
    """
    img = Image.open(image.path)

    if img.height > max_width or img.width > max_height:
        output_size = (max_width, max_height)
        img.thumbnail(output_size)
        img.save(image.path)


class BaseService(models.Model):
    """Base class for Category, Service, Case, VideoFile and Stock models"""
    seo_title = models.CharField(max_length=200, blank=True, verbose_name='SEO title')
    seo_description = models.CharField(max_length=250, blank=True, verbose_name='SEO description')
    h1 = models.CharField(max_length=150, blank=True, verbose_name='H1 тэг')

    title = models.CharField(max_length=150, unique=True, verbose_name='наименование')
    create_time = models.DateTimeField(default=timezone.now, verbose_name="время создания")
    update_time = models.DateTimeField(default=timezone.now, verbose_name="время изменения")
    is_active = models.BooleanField(default=True, db_index=True, verbose_name="активен")
    slug = models.SlugField(max_length=255, unique=True, db_index=True, verbose_name="URL")

    class Meta:
        """Specifying an abstract class."""
        abstract = True

    def __str__(self):
        """Forms and returns a printable representation of the object.
        """
        return str(self.title)

    def save(self, *args, slugified_field=None, **kwargs):
        """Automatic filling in 'update_time' and 'slug' fields when saving."""
        self.update_time = timezone.now()
        if slugified_field:
            try:
                with transaction.atomic():
                    self.slug = old_slug = slugify(slugified_field)
                    super().save(*args, **kwargs)
            except IntegrityError:
                with transaction.atomic():
                    self.slug = slugify(slugified_field + str(timezone.now()))
                    logger.info('Non-unique slug %s replaced with %s', old_slug, self.slug)
                    super().save(*args, **kwargs)
        else:
            super().save(*args, **kwargs)

    def get_absolute_url(self, urlpattern_name: str):
        """Returns formed url for the object."""
        return reverse(urlpattern_name, args=[str(self.slug)])


class Category(BaseService, MPTTModel):
    """The model for the category."""
    description = models.TextField(verbose_name="описание", blank=True, max_length=1500)
    image = models.ImageField(verbose_name="картинка", upload_to=form_image_path, blank=True)
    parent = TreeForeignKey('self', on_delete=models.PROTECT, null=True, blank=True,
                            related_name='children', db_index=True,
                            verbose_name='родительская категория')

    class MPTTMeta:
        """Ordering objects by their title."""
        order_insertion_by = ['title']

    class Meta:
        """Ordering categories according to their id. Defining the uniqueness condition."""
        unique_together = [['parent', 'slug']]
        ordering = ('id',)
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def save(self, *args, **kwargs):
        """Automatic filling in update_time and the slug field when saving,
        as well as image processing.
        Automatic activation/deactivation of child categories and services when
        activating/deactivating a category.
        """
        super().save(*args, slugified_field=self.title, **kwargs)
        if self.image:
            image_processing(self.image, 240, 240)
        for item in self.get_children():
            self.child_repeat_after_parent(item, self)
            for service in item.service_set.all():
                self.child_repeat_after_parent(service, item)
        for service in self.service_set.all():
            self.child_repeat_after_parent(service, self)

    @staticmethod
    def child_repeat_after_parent(child, parent):
        child.is_active = False if parent.is_active is False else True
        child.save()

    def get_absolute_url(self, urlpattern_name='category_read'):
        """Returns formed url for the object."""
        return super().get_absolute_url(urlpattern_name=urlpattern_name)


class Service(BaseService):
    """The model for the service."""
    body = models.TextField(verbose_name="описание услуги")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name="категория", default='1')
    image = models.ImageField(upload_to=form_image_path, blank=True, verbose_name="картинка")
    min_price = models.DecimalField(max_digits=8, decimal_places=2,
                                    default=1500.00, verbose_name="мин. стоимость")
    max_price = models.DecimalField(max_digits=8, decimal_places=2,
                                    default=15000.00, verbose_name="макс. стоимость")

    class Meta:
        """Ordering services according to their title."""
        ordering = ('title',)
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'

    def save(self, *args, **kwargs):
        """Automatic filling in update_time and the slug field when saving,
        as well as image processing."""
        super().save(*args, slugified_field=self.title, **kwargs)
        if self.image:
            image_processing(self.image, 240, 240)

    def get_absolute_url(self, urlpattern_name='service_read'):
        """Returns formed url for the object."""
        return super().get_absolute_url(urlpattern_name=urlpattern_name)


class Car(models.Model):
    """The model for the client's car."""
    brand = models.CharField(max_length=50, verbose_name='марка автомобиля')
    car_model = models.CharField(max_length=200, verbose_name='модель автомобиля')
    vin = models.CharField(max_length=17, blank=True, verbose_name='VIN автомобиля',
                           validators=[alphanumeric])
    release_year = models.PositiveSmallIntegerField(blank=True, default=2020,
                                                    verbose_name='год выпуска')
    licence_plate = models.CharField(max_length=10, blank=True,
                                     verbose_name='номера автомобиля', validators=[alphanumeric])
    description = models.CharField(max_length=200, blank=True,
                                   verbose_name='дополнительная информация об автомобиле')
    mileage = models.PositiveIntegerField(blank=True, null=True, verbose_name='пробег')
    is_active = models.BooleanField(default=True, db_index=True, verbose_name="активен")
    create_time = models.DateTimeField(default=timezone.now, verbose_name="время создания")
    update_time = models.DateTimeField(default=timezone.now, verbose_name="время изменения")

    class Meta:
        """Ordering objects according to their update time.
        """
        ordering = ('update_time',)
        verbose_name = 'Автомобиль клиента'
        verbose_name_plural = 'Автомобили клиентов'

    def __str__(self):
        """Forms and returns a printable representation of the object.
        """
        return f'{self.brand} {self.car_model}'

    def save(self, *args, **kwargs):
        """Automatic filling in 'update_time' field when saving."""
        super().save(*args, **kwargs)
        self.update_time = timezone.now()


class Case(BaseService):
    """The model for the completed work case."""
    body = models.TextField(verbose_name='описание')
    end_date = models.DateField(default=localdate, verbose_name="дата окончания работ")
    services = models.ManyToManyField(Service, verbose_name="задействованные услуги", blank=True)
    image_before = models.ImageField(upload_to=form_image_path, blank=True, verbose_name="фото до")
    image_after = models.ImageField(upload_to=form_image_path, blank=True,
                                    verbose_name="фото после")
    price = models.DecimalField(max_digits=8, decimal_places=2, default=1500.00,
                                verbose_name='стоимость')
    period = models.PositiveIntegerField(default=0, verbose_name='длительность работ в днях')
    car = models.ForeignKey(Car, on_delete=models.CASCADE, blank=True, null=True,
                            verbose_name="связанный автомобиль")

    class Meta:
        """Ordering cases according to their title.
        """
        ordering = ('update_time',)
        verbose_name = 'Выполненный заказ'
        verbose_name_plural = 'Выполненные заказы'

    def __str__(self):
        """Forms and returns a printable representation of the object.
        Returns title of the objects and date of completion of works.
        """
        return f'{super().__str__()} | {self.end_date}'

    def save(self, *args, **kwargs):
        """Automatic filling in update_time and the slug field when saving,
        as well as image processing."""
        super().save(*args, slugified_field=self.title, **kwargs)
        for image in (self.image_before, self.image_after):
            try:
                image_processing(image, 726, 484)
            except ValueError as error:
                logger.error('Image processing error for a new case %s \n', self.title, error)

    def period_to_days(self):
        """Returns the correct form of the 'день' word."""
        if self.period % 10 == 1 and abs(self.period) % 100 != 11:
            return 'день'
        if self.period % 10 in (2, 3, 4) and abs(self.period) % 100 not in (12, 13, 14):
            return 'дня'
        return 'дней'

    def get_absolute_url(self, urlpattern_name='case_read'):
        """Returns formed url for the object."""
        return super().get_absolute_url(urlpattern_name=urlpattern_name)


class STOStaff(models.Model):
    """The model for the Carmaster's staff profile."""
    create_time = models.DateTimeField(verbose_name="время создания", default=timezone.now)
    update_time = models.DateTimeField(verbose_name="время изменения", default=timezone.now)
    is_active = models.BooleanField(verbose_name="активен", default=True, db_index=True)
    first_name = models.CharField(max_length=50, verbose_name='имя')
    last_name = models.CharField(max_length=50, verbose_name='фамилия')
    patronymic = models.CharField(max_length=50, blank=True, verbose_name='отчество')
    position = models.CharField(max_length=100, verbose_name='должность')
    range = models.PositiveIntegerField(default=1, verbose_name='определение порядка вывода')
    about = models.CharField(max_length=250, blank=True, verbose_name='о сотруднике')
    image = models.ImageField(upload_to=form_image_path, blank=True, verbose_name="фото сотрудника")

    def __str__(self):
        """Forms a printable representation of the object.
        Returns the full name of the person.
        """
        return f'{self.last_name} {self.first_name}'

    class Meta:
        """Ordering staff according to their range.
        """
        ordering = ('range',)
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'

    def save(self, *args, **kwargs):
        """Automatic image processing and filling in 'update_time' and field when saving."""
        super().save(*args, **kwargs)
        self.update_time = timezone.now()
        if self.image:
            image_processing(self.image, 350, 350)


class VideoFile(BaseService):
    """The model for the videofile."""
    video = EmbedVideoField(verbose_name='ссылка на видео на YouTube')
    description = models.TextField(blank=True, verbose_name='описание')
    related_case = models.ForeignKey(Case, on_delete=models.CASCADE, blank=True, null=True,
                                     verbose_name="связанная работа")
    person = models.CharField(max_length=200, blank=True, verbose_name='хозяин авто (автор отзыва)')

    class Meta:
        """Ordering objects according to their update time.
        """
        ordering = ('update_time',)
        verbose_name = 'Видео-отзыв'
        verbose_name_plural = 'Видео-отзывы'

    def save(self, *args, **kwargs):
        """
        Automatic filling in update_time and the slug field when saving.
        Generates the correct link to the video.
        This is necessary due to the features of x-frame options for correct
        video output on the page and the ability to view it without going to the YouTube site.

        Example:
            * original link - https://www.youtube.com/watch?v=J9cRw5aMy-c
            * resulting link - https://www.youtube.com/embed/J9cRw5aMy-c
        """
        try:
            self.video = 'https://www.youtube.com/embed/' + self.video.split('=')[1]
        except IndexError:
            logger.error('Processing link error for the video file %s %s', self.title, self.video)
        super().save(*args, slugified_field=self.title, **kwargs)


class Stock(BaseService):
    """The model for the stock."""
    body = models.TextField(verbose_name='описание')
    conditions = models.TextField(verbose_name='условия', blank=True)
    is_indefinite = models.BooleanField(verbose_name="бессрочная", default=False,)
    start_date = models.DateTimeField(verbose_name="дата начала", blank=True,
                                      null=True, default=timezone.now)
    end_date = models.DateTimeField(verbose_name="дата окончания", blank=True,
                                    null=True, default=timezone.now)
    image = models.ImageField(upload_to=form_image_path, blank=True, verbose_name="фон баннера")
    services = models.ManyToManyField(Service, blank=True, verbose_name="услуги, на которые акция")

    class Meta:
        """Ordering stocks according to end date descending.
        """
        ordering = (F('end_date').desc(),)
        verbose_name = 'Акция'
        verbose_name_plural = 'Акции'

    def save(self, *args, **kwargs):
        """Automatic filling in update_time and the slug field when saving,
        as well as image processing."""
        super().save(*args, slugified_field=self.title, **kwargs)
        if self.is_indefinite:
            self.start_date, self.end_date = None, None
        if self.image:
            image_processing(self.image, 726, 300)

    def get_absolute_url(self, urlpattern_name='stock_detail'):
        """Returns formed url for the object."""
        return super().get_absolute_url(urlpattern_name=urlpattern_name)


class ClientMessages(models.Model):
    """The model for the client messages."""
    body = models.TextField(verbose_name='содержимое сообщения')
    user = models.CharField(max_length=150, verbose_name='имя пользователя')
    create_time = models.DateTimeField(verbose_name="время создания")
    update_time = models.DateTimeField(default=timezone.now, verbose_name="время изменения")
    phone = PhoneNumberField(verbose_name="номер телефона")
    processed = models.BooleanField(default=False, db_index=True, verbose_name="обработан")
    chosen_services = models.ManyToManyField(Service,
                                             verbose_name="интересующие услуги", blank=True)

    class Meta:
        """Ordering client messages according to create time descending.
        """
        ordering = (F('create_time').desc(),)
        verbose_name = 'Сообщение клиента'
        verbose_name_plural = 'Сообщения клиентов'

    def __str__(self):
        """Forms and returns a printable representation of the object.
        """
        return f'Сообщение пользователя {self.user} от {self.create_time}'

    def save(self, *args, **kwargs):
        """Automatic filling in 'update_time' field when saving.
        Replacement of the first digit of the phone number in accordance
        with the international code standard."""
        super().save(*args, **kwargs)
        self.update_time = timezone.now()
        if self.phone:
            try:
                if str(self.phone)[0] == '8':
                    self.phone = str(self.phone).replace('8', '+7', 1)
            except IndexError:
                logger.error('Phone number %s processing error for the client %s message',
                             self.phone, self.user)
