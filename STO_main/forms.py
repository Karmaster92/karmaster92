"""
Contains the forms used in the project application.
"""
import logging

from django import forms
from django.utils.translation import gettext_lazy as _
from phonenumber_field.formfields import PhoneNumberField

logger = logging.getLogger(__name__)


class ContactUsForm(forms.Form):
    """A form for messages from potential customers."""
    content = forms.CharField(widget=forms.Textarea(attrs={'cols': 60, 'rows': 10}),
                              label='Текст сообщения')
    name = forms.CharField(max_length=100, label='Как к вам обратиться?')
    phone = PhoneNumberField(region="RU",
                             widget=forms.TextInput
                             (attrs={'placeholder': _('Введите номер телефона')}),
                             label=_("Ваш номер телефона"))

    def __init__(self, *args, **kwargs):
        """Defines placeholders for form fields."""
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder'] = 'Введите ваше ФИО'
        self.fields['content'].widget.attrs['placeholder'] = 'Введите текст сообщения'
