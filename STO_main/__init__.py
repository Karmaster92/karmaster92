"""
The main application of the project.
Implements the logic of the tree architecture of **Categories** and related **Services**.

In addition, the application structure contains the following entities
and the principles of their interaction:

    * Case (an order made in the past, with a photo before/after and a description);
    * STOStaff (for each employee that the company wants to present on its website);
    * VideoFile (customer's video feedback about the company):
    * Stock(promotion that the company arranges to attract customers):


At this stage, two more auxiliary structures have been created for further scaling
and complementing the web project:

    * Car (an entity for storing information about the customer's car - can be used when creating a new video review or a completed order);
    * ClientMessages (all messages that site visitors send via the pre-registration or feedback form will be saved. This allows you to track whether these requests have been processed and prevents the manager from skipping them.);
"""
