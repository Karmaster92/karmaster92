"""Contains classes for STO_main sitemap."""

from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse
from .models import Category, Service, Case, Stock

class StaticViewSitemap(Sitemap):
    """Class for static pages sitemap."""
    changefreq = 'daily'
    priority = 0.5

    def items(self):
        """Returns a list of static urls names."""
        return ['index', 'about', 'cases_list', 'stocks_list',
                'referral', 'bonus', 'reviews', 'guarantees',
                'contacts']

    def location(self, item):
        """Returns urls formed from static urls names."""
        return reverse(item)


class DynamicBaseViewSitemap(Sitemap):
    """Base class for dynamic pages sitemap."""
    changefreq = 'daily'
    priority = 0.5
    model = None

    def items(self):
        """Returns a queryset of objects which pages need to add in sitemap."""
        return self.model.objects.filter(is_active=True)

    def lastmod(self, obj):
        """Returns and forms time of last modifying of object."""
        return obj.update_time


class DynamicCategoryViewSitemap(DynamicBaseViewSitemap):
    """Class for dynamic Category pages sitemap."""
    model = Category


class DynamicServiceViewSitemap(DynamicBaseViewSitemap):
    """Class for dynamic Service pages sitemap."""
    model = Service


class DynamicCaseViewSitemap(DynamicBaseViewSitemap):
    """Class for dynamic Case pages sitemap."""
    model = Case


class DynamicStockViewSitemap(DynamicBaseViewSitemap):
    """Class for dinamic Stock pages sitemap."""
    model = Stock
