# Car service "Кармастер" website

## Description
A website for a car service that provides a wide range of car repair services, as well as assistance in selecting a new car.
Visitors can get acquainted with information about the services and activities of the car service, choose the ones they are interested in and apply for a preliminary appointment.
The admin panel allows the site owner to add categories of tree nesting, services, information about employees, video reviews of the work performed, the work performed with photos before and after. Owner can also create promotions, with or without expiration dates, with or without conditions. In addition, there are a number of technical entities used for the convenience of the site owner. They can be the basis for further expansion of the functionality of the site (personal account, accounting of users, their cars and services performed, etc.)

## Roadmap
In the future, the site can be supplemented with a personal account, integration with the accounting system, a store of consumables.

## Authors
@LaLluviadelJulio
inspiracion@yandex.ru

